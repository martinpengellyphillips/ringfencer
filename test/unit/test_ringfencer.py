# :coding: utf-8
# :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

import math

import pytest

from ringfencer import ringfence, Point, Client, distance_between, EARTH_RADIUS


EARTH_CIRCUMFERENCE = 2 * math.pi * EARTH_RADIUS
NORTH_POLE = Point(90, 0)
SOUTH_POLE = Point(-90, 0)
CLIENTS = [
    Client(1, 'a', Point(0, 1)),
    Client(2, 'b', Point(5, 0)),
    Client(3, 'c', Point(0, 5)),
    Client(4, 'd', Point(1, 0))
]


@pytest.fixture(scope='session')
def origin():
    '''Return common origin point.'''
    return Point(0, 0)


@pytest.mark.parametrize('distance, clients, expected', [
    (100, [], []),
    (10, CLIENTS, []),
    (1000, CLIENTS, [1, 2, 3, 4]),
    (120, CLIENTS, [1, 4])
], ids=[
    'empty clients list',
    'no matching clients',
    'all matching clients',
    'clients subset'
])
def test_ringfence(origin, distance, clients, expected):
    '''Select clients based on distance from origin point.'''
    selected = list(ringfence(origin, distance, clients))
    assert (
        set([client.identifier for client in selected]) ==
        set(expected)
    )


@pytest.mark.parametrize('point_a, point_b, expected', [
    (Point(0, 0), Point(0, 0), 0),
    (Point(0, 180), Point(0, 0), EARTH_CIRCUMFERENCE / 2),
    (Point(0, 0), Point(0, 1), 111.2),
    (NORTH_POLE, SOUTH_POLE, EARTH_CIRCUMFERENCE / 2),
    (Point(0, 180), Point(0, -180), 0),
], ids=[
    'coincident points',
    'antipodal points',
    'distinct points',
    'poles',
    'equivalence of positive and negative longitude'
])
def test_distance_between(point_a, point_b, expected):
    '''Compute distance between two points on Earth\'s surface.'''
    assert round(distance_between(point_a, point_b), 2) == round(expected, 2)
