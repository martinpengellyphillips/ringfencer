# :coding: utf-8
# :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

import inspect

import pytest

from ringfencer import Client, Point
import ringfencer.entry_point


@pytest.mark.parametrize('data, expected', [
    (
        {'user_id': 1, 'name': 'a', 'latitude': 1, 'longitude': 2},
        Client(identifier=1, name='a', location=Point(1.0, 2.0))
    ),
    (
        {'user_id': 1, 'name': 'a', 'latitude': '1', 'longitude': 2},
        Client(identifier=1, name='a', location=Point(1.0, 2.0))
    ),
    ({}, ValueError),
    (
        {'user_id': 1, 'name': '', 'latitude': 1, 'longitude': 2},
        ValueError
    ),
    (
        {'user_id': 1, 'name': 'a', 'latitude': 'a', 'longitude': 2},
        ValueError
    )
], ids=[
    'valid data',
    'valid data conversion',
    'missing field',
    'null field value',
    'invalid field type'
])
def test_convert_data_to_client(data, expected):
    '''Convert data to client instance.'''
    if inspect.isclass(expected) and issubclass(expected, Exception):
        with pytest.raises(expected):
           ringfencer.entry_point.convert_data_to_client(data) == expected

    else:
        assert (
            repr(ringfencer.entry_point.convert_data_to_client(data)) ==
            repr(expected)
        )


def test_load_clients_from_file(client_list_file_object):
    '''Load clients from file.'''
    clients = list(
        ringfencer.entry_point.load_clients_from_file(client_list_file_object)
    )
    assert sorted([client.identifier for client in clients]) == [1, 2, 3, 5]


def test_load_clients_from_file_with_custom_on_error(client_list_file_object):
    '''Load clients from file collecting errors with custom callback.'''
    collected = []

    def collect(error):
        '''Collect errors.'''
        collected.append(str(error))

    clients = list(
        ringfencer.entry_point.load_clients_from_file(
            client_list_file_object,
            on_error=collect
        )
    )
    assert sorted([client.identifier for client in clients]) == [1, 2, 3, 5]
    assert collected == [
        'No JSON object could be decoded',
        "'latitude' field value u'invalid' could not be converted to a float."
    ]
