# :coding: utf-8
# :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

import os
import tempfile
from StringIO import StringIO

import pytest


@pytest.fixture()
def temporary_file(request):
    '''Return temporary file.'''
    file_handle, path = tempfile.mkstemp(**kwargs)
    os.close(file_handle)

    def cleanup():
        '''Remove temporary file.'''
        try:
            os.remove(path)
        except OSError:
            pass

    request.addfinalizer(cleanup)
    return path


@pytest.fixture()
def client_list_file_object(request):
    '''Return client list file object containing valid and invalid entries.'''
    file_object = StringIO()
    file_object.writelines([
        '{"user_id": 1, "name": "a", "latitude": "0", "longitude": "1"}\n',
        '{"user_id": 2, "name": "b", "latitude": "5", "longitude": "0"}\n',
        'NOT_JSON\n',
        '{"user_id": 3, "name": "c", "latitude": "0.0", "longitude": "5.0"}\n',
        '{"user_id": 4, "name": "d", "latitude": "invalid", "longitude": "0"}\n',
        '{"user_id": 5, "name": "e", "latitude": "1", "longitude": "0"}'
    ])
    file_object.seek(0)

    return file_object
