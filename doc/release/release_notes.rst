..
    :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 0.1.0
    :date: 2016-03-15

    .. change:: new

        Initial release featuring :ref:`using/library` and
        :ref:`using/command_line` interfaces for selecting clients based on
        distance from a specified location.
