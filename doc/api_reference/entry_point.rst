..
    :copyright: Copyright (c) 2016 Martin Pengelly-Phillips

**********************
ringfencer.entry_point
**********************

.. automodule:: ringfencer.entry_point
